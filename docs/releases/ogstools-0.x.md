# OGSTools 0.x Release Notes (upcoming release)

This is not released yet!

## API breaking changes

- meshseries.probe now squeezes the returned array: it seems more intuitive
  to return a 1D array if no list of points is provided (just a single tuple)

## Bugfixes

## Features

- plot.line now automatically sorts the data

## Infrastructure
